/**
* @file MedianContiner.h
*
* @brief Declaration of the CMedianContailer templated class
*
* @ingroup Container
*
* @author Boris Nedkov
* Contact: 
*
*/


#ifndef __MEDIANCONTAINER_H__
#define __MEDIANCONTAINER_H__

#include "DoubleLLNode.h"

namespace Container
{

/**
 * Implementation CMedianContainer value calculator class
 *
 * This implements calculation of a Median Value for numeric types
 *
 */
template <typename T> class CMedianContainer
{
public:

    /** Constructor.
    *
    * @param elem new element to add
    */
    CMedianContainer()
        : m_medianValue(NULL)
    {
    }

    ~CMedianContainer()
    {
        delete m_medianValue;
    }

    /** Add new element to Median value.
    *
    * This will force recalculation of the median value
    *
    * @param elem new element to add
    */
    void AddElement(T elem);

    /** Get the current Median value.
    *
    *
    *
    * @retval T current median value
    */
    T GetMedianValue() const;
private:
    CDoubleLLNode<T> *m_medianValue;  // current median value element

};

/**
* see description in class
*/
template <typename T> void CMedianContainer<T>::AddElement(T elem)
{
    // if no elements create the first one, otherwise add it to the list and reassign the median        
    if (m_medianValue == NULL)
    {
        m_medianValue = new CDoubleLLNode<T>(elem);
    }
    else
    {
        m_medianValue = m_medianValue->InsertValue(elem);
    }
}

/**
* see description in class
*/
template <typename T> T CMedianContainer<T>::GetMedianValue() const
{
    T ret;
    // if even number the median is the average between the two middle ones
    if (!m_medianValue)
    {
        // NO ELEMENTS. ASSERT?
    }
    else
    {
        switch  (m_medianValue->GetBias())
        {
            case MB_None:
                ret = m_medianValue->GetValue();
                break;
            case MB_Left:
                if (m_medianValue->GetPrevious())
                {
                   ret = m_medianValue->GetPrevious()->GetValue();
                }
                else
                {
                    // THE LIST IS BROKEN. ASSERT?
                }
                ret = (ret + m_medianValue->GetValue()) / 2;
                break;
            case MB_Right:
                if (m_medianValue->GetNext())
                {
                   ret = m_medianValue->GetNext()->GetValue();
                }
                else
                {
                    // THE LIST IS BROKEN. ASSERT?
                }
                ret = (ret + m_medianValue->GetValue()) / 2;
                break;
        }
    }

    return ret;
}
        
}

#endif //__MEDIANCONTAINER_H__
