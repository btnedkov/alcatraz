/**
* @file MedianContiner.h
*
* @brief Implementation of the CDoubleLLNode templated class
*
* @ingroup Container
*
* @author Boris Nedkov
* Contact: 
*
*/


#ifndef __DOUBLELLNODE_H__
#define __DOUBLELLNODE_H__


namespace Container
{

/**
 * Median bias
 *
 * This is used to indicate whether the median value is biased (i.e. there are even number of elements)
 * and if so to which direction is the bias
 *
 */
enum EMedianBias
{
    MB_None,
    MB_Left,
    MB_Right
};

/**
 * Implementation CDoubleLLNode class
 *
 * This implements double linked list for compareable elements
 *
 */
template <typename T> class CDoubleLLNode
{
public:

    /** Constructor.
    *
    * @param elem new element to add
    */
    CDoubleLLNode(T value)
        : m_value(value)
        , m_nextNode(NULL)
        , m_previousNode(NULL)
        , m_bias(MB_None)
    {
    }

    ~CDoubleLLNode()
    {
        // break the list so it doesn't clear elements twice
        if (m_nextNode)
        {
            m_nextNode->SetPrevious(NULL);
        }
        if (m_previousNode)
        {
            m_previousNode->SetNext(NULL);
        }

        // clear linked elements
        delete m_nextNode;
        delete m_previousNode;
    }

    /** Insert new value into list.
    *
    * This will force recalculation of the median value
    *
    * @param elem new element to add
    *
    * @reval CDoubleLLNode the new median node
    */
    CDoubleLLNode<T>* InsertValue(T elem);

    /** Get the current value.
    *
    * @retval T current value
    */
    T GetValue() const;

    /** Get the next node.
    *
    * @retval CDoubleLLNode<T>* next node
    */
    CDoubleLLNode<T>* GetNext() const;

    /** Get the previous node.
    *
    * @retval CDoubleLLNode<T>* previous node
    */
    CDoubleLLNode<T>* GetPrevious() const;

    /** Get the current bias.
    *
    * @retval T current value
    */
    EMedianBias GetBias() const;
private:

    /** Set the next node.
    *
    * @param next next node
    */
    void SetNext(CDoubleLLNode<T> *next);

    /** Set the previous node.
    *
    * @param previous previous node
    */
    void SetPrevious(CDoubleLLNode<T> *previous);

    /** Set the current bias.
    *
    * @oaram bias the current bias
    */
    void SetBias(EMedianBias bias);

private:
    T                 m_value;         // current value
    CDoubleLLNode<T> *m_previousNode;  // previous node
    CDoubleLLNode<T> *m_nextNode;      // next node
    EMedianBias       m_bias;          // current bias (this is only valid for median node and is transfered when the median is moved)
};

/**
* see description in class
*/
template <typename T> CDoubleLLNode<T>* CDoubleLLNode<T>::InsertValue(T value)
{
    CDoubleLLNode<T>* newMedian = NULL;
    bool moveLeft = false;

    // if new element is larger/same (i.e. should be placed on the right)
    if (m_value <= value)
    {
        // check if new node should be inserted right between this element and the next
        if (!m_nextNode || m_nextNode->GetValue() > value)
        {
            // between two
            CDoubleLLNode<T> *median = new CDoubleLLNode<T>(value);
            // adjust link between new element and next node if any
            if (m_nextNode)
            {
                median->SetNext(m_nextNode);
                m_nextNode->SetPrevious(median);
            }
            // adjust link between new element and current node
            median->SetPrevious(this);
            SetNext(median);
        }
        else
        {
            // continue iteration in the right direction to find suitable place for element
            m_nextNode->InsertValue(value);
        }
        newMedian = m_nextNode;
        moveLeft = true;
    }
    // if new element is smaller (i.e. should be placed on the left)
    else
    {
        // check if new node should be inserted right between this element and the previous
        if (!m_previousNode || m_previousNode->GetValue() < value)
        {
            // between two
            CDoubleLLNode<T> *median = new CDoubleLLNode<T>(value);
            // adjust link between new element and previous node if any
            if (m_previousNode)
            {
                median->SetPrevious(m_previousNode);
                m_previousNode->SetNext(median);
            }
            // adjust link between new element and current node
            median->SetNext(this);
            SetPrevious(median);
        }
        else
        {
          // continue iteration in the left direction to find suitable place for element
            m_previousNode->InsertValue(value);
        }
        newMedian = m_previousNode;
    }

    // set bias and readjust median element
    switch (m_bias)
    {
        case MB_None:
            // was not biased so set the new bias (i.e. even number so will always be biased) 
            if (moveLeft)
            {
                m_bias = MB_Left;
            }
            else
            {
                m_bias = MB_Right;
            }
            break;
        case MB_Left:
            // was biased to the left so rewind to current element if the insert is again to the left and reset bias
            if (moveLeft)
            {
                newMedian = this;
            }
            m_bias = MB_None;
            break;
        case MB_Right:
            // was biased to the right so rewind to current element if the insert is again to the right and reset bias
            if (!moveLeft)
            {
                newMedian = this;
            }
            m_bias = MB_None;
            break;
    }

    // transfer the bias to the new median
    newMedian->SetBias(m_bias);
    
    return newMedian;
}

/**
* see description in class
*/
template <typename T> T CDoubleLLNode<T>::GetValue() const
{
    return m_value;
}

/**
* see description in class
*/
template <typename T> void CDoubleLLNode<T>::SetNext(CDoubleLLNode<T> *next)
{
    m_nextNode = next;
}

/**
* see description in class
*/
template <typename T> void CDoubleLLNode<T>::SetPrevious(CDoubleLLNode<T> *previous)
{
    m_previousNode = previous;
}

/**
* see description in class
*/
template <typename T> CDoubleLLNode<T>* CDoubleLLNode<T>::GetNext() const
{
    return m_nextNode;
}

/**
* see description in class
*/
template <typename T> CDoubleLLNode<T>* CDoubleLLNode<T>::GetPrevious() const
{
    return m_previousNode;
}


/**
* see description in class
*/
template <typename T> void CDoubleLLNode<T>::SetBias(EMedianBias bias)
{
    m_bias = bias;
}

/**
* see description in class
*/
template <typename T> EMedianBias CDoubleLLNode<T>::GetBias() const
{
    return m_bias;
}

}

#endif //__DOUBLELLNODE_H__
