#include "gtest/gtest.h"
#include "MedianContainer.h"

#include <string>

TEST(MedianTest, Test3Elements) {
    Container::CMedianContainer<int> medContainer;
    medContainer.AddElement(12);
    medContainer.AddElement(3);
    medContainer.AddElement(5);

    EXPECT_EQ(medContainer.GetMedianValue(), 5);
}
TEST(MedianTest, TestEvenElements) {
    Container::CMedianContainer<int> medContainer;
    medContainer.AddElement(2);
    medContainer.AddElement(4);

    EXPECT_EQ(medContainer.GetMedianValue(), 3);
}

TEST(MedianTest, Test3ElementsOrdered) {
    Container::CMedianContainer<int> medContainer;
    medContainer.AddElement(12);
    medContainer.AddElement(5);
    medContainer.AddElement(3);

    EXPECT_EQ(medContainer.GetMedianValue(), 5);
}

TEST(MedianTest, TestLargeSet) {
    Container::CMedianContainer<int> medContainer;

    medContainer.AddElement(3);
    medContainer.AddElement(13);
    medContainer.AddElement(7);
    medContainer.AddElement(5);
    medContainer.AddElement(21);
    medContainer.AddElement(23);
    medContainer.AddElement(39);
    medContainer.AddElement(23);
    medContainer.AddElement(40);
    medContainer.AddElement(23);
    medContainer.AddElement(14);
    medContainer.AddElement(12);
    medContainer.AddElement(56);
    medContainer.AddElement(23);
    medContainer.AddElement(29);

    EXPECT_EQ(medContainer.GetMedianValue(), 23);
}


TEST(MedianTest, TestLargeSetEven) {
    Container::CMedianContainer<int> medContainer;
    
    medContainer.AddElement(3);
    medContainer.AddElement(13);
    medContainer.AddElement(7);
    medContainer.AddElement(5);
    medContainer.AddElement(21);
    medContainer.AddElement(23);
    medContainer.AddElement(23);
    medContainer.AddElement(40);
    medContainer.AddElement(23);
    medContainer.AddElement(14);
    medContainer.AddElement(12);
    medContainer.AddElement(56);
    medContainer.AddElement(23);
    medContainer.AddElement(29);

    EXPECT_EQ(medContainer.GetMedianValue(), 22);
}
